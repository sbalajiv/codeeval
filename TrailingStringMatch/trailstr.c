#include <stdio.h>
#include <errno.h>
#include <string.h>

char file_line[1024];
char test_string[256], substring[256];

int main(int argc, char **argv) {
	FILE *fp;
	int	num_params;
	char *stptr;

	if (argc == 1) {
		printf("Usage: trailstr <file-name>\n");
		return -EAGAIN;
	}

	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("File does not exist : %s\n", argv[1]);
		return -EEXIST;
	}

	while (fgets(file_line, sizeof(file_line), fp)) {
		if (file_line[0] == '\n')
			continue;
		num_params = sscanf(file_line, "%255[^,],%255[^\n]", test_string, substring);
		if ((num_params != 2) || (num_params == EOF)) {
			printf("NA\n");
			continue;
		}
		if (strlen(substring) > strlen(test_string)){
			printf("0\n");
			continue;
		}
		stptr = test_string + (strlen(test_string) - strlen(substring));
		if (strcmp(stptr, substring) == 0) 
			printf("1\n");
		else
			printf("0\n");
	}

	fclose(fp);
}
