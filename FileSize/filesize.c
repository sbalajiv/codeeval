#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

char block[4096];	//Buffer to read 4k blocks from file

main(int argc, char *argv[]) {
	int fd;	
	int bytes_read = 0;
	unsigned int total_bytes_read = 0;

	if (argc == 1) {
		printf("Usage: filesize <file-name>\n");
		return -EINVAL;
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		printf("Unable to open file : %s\n", argv[1]);
		return -EINVAL;
	}

	while((bytes_read = read(fd, (void *)block, sizeof(block))) != 0) {
		if (bytes_read > 0) {
			total_bytes_read += (unsigned int) bytes_read;
		}
		else {
			printf("Error reading from file: %d\n", bytes_read);
			close(fd);
			return -EBUSY;
		}
	}
	close(fd);

	printf("The file size in bytes is: %u\n", total_bytes_read);
}
