#include <stdio.h>
#include <stdlib.h>

// A line that will be read from a file at a time
char line[64];

//Returns the value at the given bit position (pos)
unsigned long int getBitPositionValue(unsigned long int number, unsigned long int pos) {
	return (((number & (1 << pos)) >> pos) & 1);
}

// the function tries to reads 3 integers from line, which are comma separated
int getRequiredValuesFromString(char *line, unsigned long int *number, unsigned long int *position1, unsigned long int *position2) {
	return sscanf(line, "%lu,%lu,%lu", number, position1, position2);
}

int isBitValueSameAtPositions(unsigned long int number, unsigned long int position1, unsigned long int position2) {
	return (getBitPositionValue(number, position1) == getBitPositionValue(number, position2));
}

int main(int argc, char *argv[]) {
    FILE *fs;	//file handle
    unsigned long int number, position1, position2;

    int retval;

    if (argc == 1) {
        printf("Usage: bitpos <input-file-name>\n");
        exit(-1);
    }

    fs = fopen(argv[1], "r");
    if (fs == NULL) {
        printf("Error opening file: %s\n", argv[1]);
        exit(-2);
    }

    while (fgets(line, 64, fs)) {
        if (line[0] == '\n')
            continue;
        retval = getRequiredValuesFromString(line, &number, &position1, &position2);
        if ((retval != 3) || (retval == EOF)) {
        	printf("None\n");
            continue;
        }
        if ((position1 > (sizeof(unsigned long) * 8)) || (position2 > (sizeof(unsigned long) * 8))) {
        	printf("position1 or position2 out of bounds\n");
        	continue;
        }

        if (isBitValueSameAtPositions(number, position1, position2))
            printf("true\n");
        else
            printf("false\n");
    }

    fclose(fs);
    return 0;
}
