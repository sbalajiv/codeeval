#include <stdio.h>

main() {
	int numerator, denominator;
	int subtracted_number;
	int temporary_store;

	printf("Enter a numerator: ");
	scanf("%d", &numerator);
	printf("Enter a denominator: ");
	scanf("%d", &denominator);

	if (numerator < denominator) {
		subtracted_number = numerator;
	}
	else {
		temporary_store = numerator;
		do {
			subtracted_number = temporary_store - denominator;
			temporary_store = subtracted_number;
		}while (subtracted_number >= denominator);
	}

	printf("%d %% %d = %d\n", numerator, denominator, subtracted_number);
}
